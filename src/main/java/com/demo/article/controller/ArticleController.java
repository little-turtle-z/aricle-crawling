package com.demo.article.controller;


import com.demo.article.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小王八
 * @since 2022-09-20
 */
@RestController
@RequestMapping("/article")
public class ArticleController {

}
